﻿using Microsoft.AspNetCore.Mvc;

using MvcTermLife.Models;

namespace MvcTermLife.Controllers
{
    public class HomeController : Controller
    {
        // GET: /Home/Index 
        public IActionResult Index()
        {
            return View();
        }

        // POST: /Home/Index      
        [HttpPost]
        public ActionResult Index(Person p, Policy pol)
        {
            if (ModelState.IsValid)
            {
                decimal premium = Rate.MonthlyPremium(p.Age.Value, pol.Amount.Value);
                ViewBag.amount = string.Format("{0:C}", pol.Amount);
                ViewBag.premium = string.Format("{0:C}", premium);
                return View("Quote", p);
            }
            else
                return View();
        }

    }
}