﻿using System.ComponentModel.DataAnnotations;

namespace MvcTermLife.Models
{
    public class Policy
    {
        [Required]
        [Range(10000, 1000000)]
        public decimal? Amount { get; set; }
    }
}
