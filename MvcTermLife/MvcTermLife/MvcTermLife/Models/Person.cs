﻿using System.ComponentModel.DataAnnotations;

namespace MvcTermLife.Models
{
    public class Person
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(20, 69)]
        public int? Age { get; set; }
    }
}
